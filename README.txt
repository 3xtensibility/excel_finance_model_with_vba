Welcome to the Excel_Finance_Model_With_VBA repository of Blake Giove.


PURPOSE FOR THE REPOSITORY
To show skills in the VBA coding language to automate financial modeling for financial analysis.
The simple EBITDA financial model with scenario analysis and sensitivity analysis is built within Excel 2019 version and utilizes Microsoft Visual Basic for Applications version 7.1.


REPOSITORY EXICUTION PROCESS
There are 11 form control buttons that interact with the model that perform scenario adjustments to the model and summarize EBITDA and other financial measurements as well as summarize margins and reset the model to the original scenario.


ADDITIONAL NOTES
The technology stack includes: Windows 10, Excel 2019 version 2105, and Microsoft Visual Basic For Applications version 7.1.1109 (version 1109).
The Excel program as well as Microsoft Visual Basic for Applications on Windows 10 should be installed prior to utilizing the repository.
