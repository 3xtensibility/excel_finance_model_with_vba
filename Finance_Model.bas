Attribute VB_Name = "Finance_Model"
Sub Base_Case_and_Original_Assumptions()
    Dim cell_sb_scenario As Double
    Dim cell_qb_scenario As Double
    Dim cell_tb_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    
    wksheet.Activate
    
    With wksheet
        wksheet.Range("K12").Value = "120"
        wksheet.Range("K13").Value = "7000"
        wksheet.Range("K14").Value = "18%"
        wksheet.Range("K15").Value = "4"
        wksheet.Range("K16").Value = "-1405482"
        wksheet.Range("K17").Value = "6"
        wksheet.Range("K18").Value = "28%"
        wksheet.Range("K19").Value = "43%"
        wksheet.Range("J12").Value = "100"
        wksheet.Range("J13").Value = "5000"
        wksheet.Range("J14").Value = "20%"
        wksheet.Range("J15").Value = "6"
        wksheet.Range("J16").Value = "-1826218"
        wksheet.Range("J17").Value = "9"
        wksheet.Range("J18").Value = "18%"
        wksheet.Range("J19").Value = "18%"
        wksheet.Range("I12").Value = "90"
        wksheet.Range("I13").Value = "4000"
        wksheet.Range("I14").Value = "21%"
        wksheet.Range("I15").Value = "8"
        wksheet.Range("I16").Value = "-2428404"
        wksheet.Range("I17").Value = "14"
        wksheet.Range("I18").Value = "12%"
        wksheet.Range("I19").Value = "6%"
        
        cell_sb_scenario = wksheet.Range("J12").Value
        wksheet.Range("C4").Value = cell_sb_scenario
        
        cell_qb_scenario = wksheet.Range("J13").Value
        wksheet.Range("C13").Value = cell_qb_scenario
        
        cell_tb_scenario = wksheet.Range("J14").Value
        wksheet.Range("C16").Value = cell_tb_scenario
    End With
    
    wksheet.AutoFilterMode = False
    
End Sub
Sub Quantity_Sold_High()
    Dim cell_qh_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_qh_scenario = wksheet.Range("K13").Value
    wksheet.Range("C13").Value = cell_qh_scenario
End Sub
Sub Quantity_Sold_Base()
    Dim cell_qb_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_qb_scenario = wksheet.Range("J13").Value
    wksheet.Range("C13").Value = cell_qb_scenario
End Sub
Sub Quantity_Sold_Low()
    Dim cell_ql_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_ql_scenario = wksheet.Range("I13").Value
    wksheet.Range("C13").Value = cell_ql_scenario
End Sub
Sub Price_Sold_High()
    Dim cell_sh_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_sh_scenario = wksheet.Range("K12").Value
    wksheet.Range("C4").Value = cell_sh_scenario
End Sub
Sub Price_Sold_Base()
    Dim cell_sb_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_sb_scenario = wksheet.Range("J12").Value
    wksheet.Range("C4").Value = cell_sb_scenario
End Sub
Sub Price_Sold_Low()
    Dim cell_sl_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_sl_scenario = wksheet.Range("I12").Value
    wksheet.Range("C4").Value = cell_sl_scenario
End Sub
Sub Other_Opex_Sales_Floor_High()
    Dim cell_th_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_th_scenario = wksheet.Range("K14").Value
    wksheet.Range("C16").Value = cell_th_scenario
End Sub
Sub Other_Opex_Sales_Floor_Base()
    Dim cell_tb_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_tb_scenario = wksheet.Range("J14").Value
    wksheet.Range("C16").Value = cell_tb_scenario
End Sub
Sub Other_Opex_Sales_Floor_Low()
    Dim cell_tl_scenario As Double
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    wksheet.Activate
    cell_tl_scenario = wksheet.Range("I14").Value
    wksheet.Range("C16").Value = cell_tl_scenario
End Sub
Sub EBITDA()
    Dim summary_filter_e As Range
    Dim main_array(12)
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    Set summary_filter_e = wksheet.Range("B21:V21")
    
    With summary_filter_e
        main_array(1) = "Revenue"
        main_array(2) = "Gross profit"
        main_array(3) = "Total fixed costs"
        main_array(4) = "EBITDA"
        main_array(5) = "EBIT"
        main_array(6) = "Tax expense"
        main_array(7) = "Net profit"
        main_array(8) = "Cumulative profit"
        main_array(9) = "Cash flow"
        main_array(10) = "Cumulative cash flow"
        main_array(11) = "Rolling IRR"
        main_array(12) = "IRR"
        
        .AutoFilter Field:=1, Criteria1:=main_array, Operator:=xlFilterValues
    End With
End Sub
Sub Margins()
    Dim summary_filter_m As Range
    Dim main_array(6)
    Dim wksheet As Worksheet
    
    Set wksheet = Workbooks("Excel_Finance_Model_With_VBA.xlsm").Sheets("Model")
    Set summary_filter_m = wksheet.Range("B21:V21")
    
    With summary_filter_m
        main_array(1) = "Sales growth"
        main_array(2) = "Gross margin %"
        main_array(3) = "Other opex / sales"
        main_array(4) = "Net profit margin %"
        main_array(5) = "Rolling IRR"
        main_array(6) = "IRR"
        
        .AutoFilter Field:=1, Criteria1:=main_array, Operator:=xlFilterValues
    End With
End Sub

